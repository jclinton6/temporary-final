#include "mmma.h"
#include <petscsys.h>

struct _mmma
{
  MPI_Comm comm;
};

int MMMACreate(MPI_Comm comm, MMMA *mmma_p)
{
  MMMA mmma = NULL;
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  ierr = PetscCalloc1(1,&mmma); CHKERRQ(ierr);

  mmma->comm = comm;

  *mmma_p = mmma;
  PetscFunctionReturn(0);
}

int MMMADestroy(MMMA *mmma_p)
{
  PetscErrorCode ierr;
  PetscFunctionBeginUser;

  ierr = PetscFree(*mmma_p); CHKERRQ(ierr);

  PetscFunctionReturn(0);
}

int MMMAGetMatrixArraysDouble(MMMA mmma, size_t M, size_t N, size_t R,
                              struct MatrixBlock *cBlock,
                              struct MatrixBlock *aBlock,
                              struct MatrixBlock *bBlock,
                              double **CLocal_p, double **ALocal_p, double **BLocal_p)
{
  MPI_Comm comm;
  int      size, rank;
  PetscInt nClocal, nAlocal, nBlocal;
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  comm = mmma->comm;

  ierr = MPI_Comm_size(comm, &size); CHKERRQ(ierr);
  ierr = MPI_Comm_rank(comm, &rank); CHKERRQ(ierr);

  cBlock->rowStart = (rank * M) / size;
  cBlock->rowEnd   = ((rank + 1) * M) / size;
  cBlock->colStart = 0;
  cBlock->colEnd   = N;
  nClocal = (cBlock->rowEnd - cBlock->rowStart) * (cBlock->colEnd - cBlock->colStart);

  aBlock->rowStart = (rank * M) / size;
  aBlock->rowEnd   = ((rank + 1) * M) / size;
  aBlock->colStart = 0;
  aBlock->colEnd   = R;
  nAlocal = (aBlock->rowEnd - aBlock->rowStart) * (aBlock->colEnd - aBlock->colStart);

  bBlock->rowStart = (rank * R) / size;
  bBlock->rowEnd   = ((rank + 1) * R) / size;
  bBlock->colStart = 0;
  bBlock->colEnd   = N;
  nBlocal = (bBlock->rowEnd - bBlock->rowStart) * (bBlock->colEnd - bBlock->colStart);

  ierr = PetscMalloc3(nClocal, CLocal_p, nAlocal, ALocal_p, nBlocal, BLocal_p); CHKERRQ(ierr);

  PetscFunctionReturn(0);
}

int MMMARestoreMatrixArraysDouble(MMMA mmma, size_t M, size_t N, size_t R,
                                  struct MatrixBlock *cBlock,
                                  struct MatrixBlock *aBlock,
                                  struct MatrixBlock *bBlock,
                                  double **CLocal_p, double **ALocal_p, double **BLocal_p)
{
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  ierr = PetscFree3(*CLocal_p, *ALocal_p, *BLocal_p); CHKERRQ(ierr);

  PetscFunctionReturn(0);
}

int MMMAGetMatrixArraysSingle(MMMA mmma, size_t M, size_t N, size_t R,
                              struct MatrixBlock *cBlock,
                              struct MatrixBlock *aBlock,
                              struct MatrixBlock *bBlock,
                              float **CLocal_p, float **ALocal_p, float **BLocal_p)
{
  MPI_Comm comm;
  int      size, rank;
  PetscInt nClocal, nAlocal, nBlocal;
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  comm = mmma->comm;

  ierr = MPI_Comm_size(comm, &size); CHKERRQ(ierr);
  ierr = MPI_Comm_rank(comm, &rank); CHKERRQ(ierr);

  cBlock->rowStart = (rank * M) / size;
  cBlock->rowEnd   = ((rank + 1) * M) / size;
  cBlock->colStart = 0;
  cBlock->colEnd   = N;
  nClocal = (cBlock->rowEnd - cBlock->rowStart) * (cBlock->colEnd - cBlock->colStart);

  aBlock->rowStart = (rank * M) / size;
  aBlock->rowEnd   = ((rank + 1) * M) / size;
  aBlock->colStart = 0;
  aBlock->colEnd   = R;
  nAlocal = (aBlock->rowEnd - aBlock->rowStart) * (aBlock->colEnd - aBlock->colStart);

  bBlock->rowStart = (rank * R) / size;
  bBlock->rowEnd   = ((rank + 1) * R) / size;
  bBlock->colStart = 0;
  bBlock->colEnd   = N;
  nBlocal = (bBlock->rowEnd - bBlock->rowStart) * (bBlock->colEnd - bBlock->colStart);

  ierr = PetscMalloc3(nClocal, CLocal_p, nAlocal, ALocal_p, nBlocal, BLocal_p); CHKERRQ(ierr);

  PetscFunctionReturn(0);
}

int MMMARestoreMatrixArraysSingle(MMMA mmma, size_t M, size_t N, size_t R,
                                  struct MatrixBlock *cBlock,
                                  struct MatrixBlock *aBlock,
                                  struct MatrixBlock *bBlock,
                                  float **CLocal_p, float **ALocal_p, float **BLocal_p)
{
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  ierr = PetscFree3(*CLocal_p, *ALocal_p, *BLocal_p); CHKERRQ(ierr);

  PetscFunctionReturn(0);
}

int MMMAApplyDouble(MMMA mmma, size_t M, size_t N, size_t R, double alpha,
                    const struct MatrixBlock *cBlock,
                    const struct MatrixBlock *aBlock,
                    const struct MatrixBlock *bBlock,
                    double *C, const double *A, const double *B)
{
  return 0;
}


int MMMAApplySingle(MMMA mmma, size_t M, size_t N, size_t R, float alpha,
                    const struct MatrixBlock *cBlock,
                    const struct MatrixBlock *aBlock,
                    const struct MatrixBlock *bBlock,
                    float *C, const float *A, const float *B)
{
  return 0;
}
