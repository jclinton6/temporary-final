#!/bin/sh
#SBATCH  -J mmma-double                  # Job name
#SBATCH  -p normal                       # Queue (development or normal)
#SBATCH  -N 64                           # Number of nodes
#SBATCH --tasks-per-node 64              # Number of tasks per node
#SBATCH  -t 00:10:00                     # Time limit hrs:min:sec
#SBATCH  -A TG-TRA170035                 # Allocation
#SBATCH  -o mmma-double-%j.out           # Standard output and error log

module use /home1/01236/tisaac/opt/modulefiles
module load petsc/cse6230-double

make test_mmma

git rev-parse HEAD

git diff-files

pwd; hostname; date

ibrun tacc_affinity ./test_mmma -tests 24,26,28,30

date
