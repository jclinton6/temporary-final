const char help[] = "Test driver for the correctness of matrix-matrix multiply-add implementation";

#include <petscviewer.h>
#include <petscblaslapack.h>
#include <petscmat.h>
#include "mmma.h"

static PetscErrorCode FillPetscMatrix(Mat A, struct MatrixBlock *aBlock, PetscScalar *Aa)
{
  PetscInt *idxm, *idxn, i;
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = PetscMalloc2(aBlock->rowEnd - aBlock->rowStart, &idxm, aBlock->colEnd - aBlock->colStart, &idxn); CHKERRQ(ierr);
  for (i = 0; i < aBlock->rowEnd - aBlock->rowStart; i++) {
    idxm[i] = aBlock->rowStart + i;
  }
  for (i = 0; i < aBlock->colEnd - aBlock->colStart; i++) {
    idxn[i] = aBlock->colStart + i;
  }
  ierr = MatSetValues(A, aBlock->rowEnd - aBlock->rowStart, idxm, aBlock->colEnd - aBlock->colStart, idxn, Aa, INSERT_VALUES); CHKERRQ(ierr);
  ierr = PetscFree2(idxm,idxn); CHKERRQ(ierr);
  ierr = MatAssemblyBegin(A, MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);
  ierr = MatAssemblyEnd(A, MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);
  PetscFunctionReturn(0);
}

int main(int argc, char **argv)
{
  PetscInt       tests[4] = {20, 22, 24, 26};
  PetscScalar    fmaps[4];
  PetscInt       test, nCheck = 10, numTests = 4;
  PetscRandom    globalrand;
  PetscRandom    selfrand;
  MPI_Comm       comm;
  PetscViewer    viewer;
  MMMA           mmma = NULL;
  PetscErrorCode ierr;

  ierr = PetscInitialize(&argc, &argv, NULL, help); if (ierr) return ierr;
  comm = PETSC_COMM_WORLD;
  viewer = PETSC_VIEWER_STDOUT_WORLD;
  ierr = PetscOptionsBegin(comm, NULL, "Matrix-Matrix Multiply-Add Test Options", "test_mmma.c");CHKERRQ(ierr);
  ierr = PetscOptionsIntArray("-tests", "Test sizes to run", "test_mmma.c", tests, &numTests, NULL);CHKERRQ(ierr);
  ierr = PetscOptionsInt("-num_time", "The number of times a test is timed", "test_mmma.c", nCheck, &nCheck, NULL);CHKERRQ(ierr);
  ierr = PetscOptionsEnd();CHKERRQ(ierr);

  if (!numTests) {
    numTests = 4;
  }

  ierr = PetscRandomCreate(comm, &globalrand);CHKERRQ(ierr);
  ierr = PetscRandomSetFromOptions(globalrand);CHKERRQ(ierr);
  ierr = PetscRandomSeed(globalrand);CHKERRQ(ierr);
  ierr = PetscRandomCreate(PETSC_COMM_SELF, &selfrand);CHKERRQ(ierr);
  ierr = PetscRandomSetFromOptions(selfrand);CHKERRQ(ierr);
  ierr = PetscRandomSeed(selfrand);CHKERRQ(ierr);

  ierr = MMMACreate(comm, &mmma); CHKERRQ(ierr);

#if defined(PETSC_USE_REAL_DOUBLE)
  ierr = PetscViewerASCIIPrintf(viewer, "Running %D tests of MMMAApplyDouble()\n", numTests);CHKERRQ(ierr);
#elif defined(PETSC_USE_REAL_SINGLE)
  ierr = PetscViewerASCIIPrintf(viewer, "Running %D tests of MMMAApplySingle()\n", numTests);CHKERRQ(ierr);
#else
  SETERRQ(comm,PETSC_ERR_LIB,"Precision not supported by test");
#endif
  ierr = PetscViewerASCIIPushTab(PETSC_VIEWER_STDOUT_WORLD);
  for (test = 0; test < numTests; test++) {
    Mat                A, B, C, D, E;
    PetscScalar       *Aa, *Ba, *Ca, *CaCopy;
    PetscReal          logm, logn, logr, alpha;
    struct MatrixBlock cBlock, aBlock, bBlock;
    PetscInt           m, n, r, i, nClocal, nAlocal, nBlocal;
    PetscScalar        totalTime = 0.;

    ierr = PetscViewerASCIIPrintf(viewer, "Test %D: scale %D\n", test, tests[test]);CHKERRQ(ierr);
    ierr = PetscViewerASCIIPushTab(PETSC_VIEWER_STDOUT_WORLD);
    logm = tests[test] / 2.;
    logn = tests[test] - logm;

    ierr = PetscRandomSetInterval(selfrand, PetscMin(logn, logm) - 2., PetscMin(logn, logm));CHKERRQ(ierr);
    ierr = PetscRandomGetValueReal(selfrand, &logr);CHKERRQ(ierr);

    m = (PetscInt) PetscPowReal(2., logm);
    n = (PetscInt) PetscPowReal(2., logn);
    r = (PetscInt) PetscPowReal(2., logr);

    ierr = PetscViewerASCIIPrintf(viewer, "Test dimensions: C = [%D x %D], A = [%D x %D], B = [%D x %D]\n", m, n, m, r, r, n);CHKERRQ(ierr);

#if defined(PETSC_USE_REAL_DOUBLE)
    ierr = MMMAGetMatrixArraysDouble(mmma, m, n, r, &cBlock, &aBlock, &bBlock, &Ca, &Aa, &Ba); CHKERRQ(ierr);
#else
    ierr = MMMAGetMatrixArraysSingle(mmma, m, n, r, &cBlock, &aBlock, &bBlock, &Ca, &Aa, &Ba); CHKERRQ(ierr);
#endif
    nClocal = (cBlock.rowEnd - cBlock.rowStart) * (cBlock.colEnd - cBlock.colStart);
    nAlocal = (aBlock.rowEnd - aBlock.rowStart) * (aBlock.colEnd - aBlock.colStart);
    nBlocal = (bBlock.rowEnd - bBlock.rowStart) * (bBlock.colEnd - bBlock.colStart);

    ierr = PetscRandomSetInterval(globalrand, -1., 1.);CHKERRQ(ierr);

    ierr = PetscRandomGetValue(selfrand, &alpha);CHKERRQ(ierr);

    ierr = MatCreateDense(comm, PETSC_DECIDE, PETSC_DECIDE, m, n, NULL, &D); CHKERRQ(ierr);
    ierr = MatCreateDense(comm, PETSC_DECIDE, PETSC_DECIDE, m, n, NULL, &E); CHKERRQ(ierr);
    ierr = MatCreateDense(comm, PETSC_DECIDE, PETSC_DECIDE, m, r, NULL, &A); CHKERRQ(ierr);
    ierr = MatCreateDense(comm, PETSC_DECIDE, PETSC_DECIDE, r, n, NULL, &B); CHKERRQ(ierr);

    for (i = 0; i < nAlocal; i++) {
      ierr = PetscRandomGetValue(globalrand, &Aa[i]);CHKERRQ(ierr);
    }
    ierr = FillPetscMatrix(A, &aBlock, Aa); CHKERRQ(ierr);
    for (i = 0; i < nBlocal; i++) {
      ierr = PetscRandomGetValue(globalrand, &Ba[i]);CHKERRQ(ierr);
    }
    ierr = FillPetscMatrix(B, &bBlock, Ba); CHKERRQ(ierr);
    for (i = 0; i < nClocal; i++) {
      ierr = PetscRandomGetValue(globalrand, &Ca[i]);CHKERRQ(ierr);
    }
    ierr = FillPetscMatrix(D, &cBlock, Ca); CHKERRQ(ierr);
    {
      Mat AB;

      ierr = MatScale(D, alpha); CHKERRQ(ierr);
      ierr = MatMatMult(A, B, MAT_INITIAL_MATRIX, PETSC_DEFAULT, &AB); CHKERRQ(ierr);
      ierr = MatAXPY(D,1.,AB,SAME_NONZERO_PATTERN); CHKERRQ(ierr);
      ierr = MatDestroy(&AB); CHKERRQ(ierr);
    }

    {
      PetscScalar *Aar, *Bar, *Ea;
      PetscInt localm, localr;
      Mat AB;

      ierr = MatDenseGetArray(A, &Aar); CHKERRQ(ierr);
      ierr = MatDenseGetArray(B, &Bar); CHKERRQ(ierr);
      ierr = MatGetLocalSize(A, &localm, NULL); CHKERRQ(ierr);
      ierr = MatGetLocalSize(B, &localr, NULL); CHKERRQ(ierr);

      for (i = 0; i < localm * r; i++) {
        Aar[i] = PetscAbsScalar(Aar[i]);
      }
      for (i = 0; i < localr * n; i++) {
        Bar[i] = PetscAbsScalar(Bar[i]);
      }

      ierr = MatDenseRestoreArray(B, &Bar); CHKERRQ(ierr);
      ierr = MatDenseRestoreArray(A, &Aar); CHKERRQ(ierr);

      ierr = PetscMalloc1(nClocal, &Ea); CHKERRQ(ierr);
      for (i = 0; i < nClocal; i++) {
        Ea[i] = PetscAbsScalar(Ca[i]);
      }

      ierr = FillPetscMatrix(E, &cBlock, Ea); CHKERRQ(ierr);
      ierr = PetscFree(Ea); CHKERRQ(ierr);

      ierr = MatScale(E, PetscAbsScalar(alpha)); CHKERRQ(ierr);
      ierr = MatMatMult(A, B, MAT_INITIAL_MATRIX, PETSC_DEFAULT, &AB); CHKERRQ(ierr);
      ierr = MatAXPY(E,1.,AB,SAME_NONZERO_PATTERN); CHKERRQ(ierr);
      ierr = MatDestroy(&AB); CHKERRQ(ierr);
    }

    ierr = PetscMalloc1(nClocal, &CaCopy); CHKERRQ(ierr);
    ierr = PetscMemcpy(CaCopy, Ca, nClocal * sizeof (PetscScalar)); CHKERRQ(ierr);

    ierr = MatDestroy(&B); CHKERRQ(ierr);
    ierr = MatDestroy(&A); CHKERRQ(ierr);

    for (i = 0; i < nCheck + 1; i++) {
      double tic, toc, total, maxTotal = 0.;

      ierr = PetscMemcpy(Ca, CaCopy, nClocal * sizeof (PetscScalar)); CHKERRQ(ierr);
      ierr = MPI_Barrier(comm); CHKERRQ(ierr);
      tic = MPI_Wtime();
#if defined(PETSC_USE_REAL_DOUBLE)
      ierr = MMMAApplyDouble(mmma, m, n, r, alpha, &cBlock, &aBlock, &bBlock, Ca, Aa, Ba); CHKERRQ(ierr);
#else
      ierr = MMMAApplySingle(mmma, m, n, r, alpha, &cBlock, &aBlock, &bBlock, Ca, Aa, Ba); CHKERRQ(ierr);
#endif
      toc = MPI_Wtime();
      total = toc - tic;
      ierr = MPI_Allreduce(&total, &maxTotal, 1, MPI_DOUBLE, MPI_MAX, comm); CHKERRQ(ierr);
      if (i) {totalTime += maxTotal;}
    }
    totalTime /= nCheck;

    ierr = PetscFree(CaCopy); CHKERRQ(ierr);
    ierr = MatCreateDense(comm, PETSC_DECIDE, PETSC_DECIDE, m, n, NULL, &C); CHKERRQ(ierr);
    ierr = FillPetscMatrix(C, &cBlock, Ca); CHKERRQ(ierr);
    ierr = MatAXPY(D,-1.,C,SAME_NONZERO_PATTERN); CHKERRQ(ierr);
    ierr = MatDestroy(&C); CHKERRQ(ierr);

#if defined(PETSC_USE_REAL_DOUBLE)
    ierr = MMMARestoreMatrixArraysDouble(mmma, m, n, r, &cBlock, &aBlock, &bBlock, &Ca, &Aa, &Ba); CHKERRQ(ierr);
#else
    ierr = MMMARestoreMatrixArraysSingle(mmma, m, n, r, &cBlock, &aBlock, &bBlock, &Ca, &Aa, &Ba); CHKERRQ(ierr);
#endif
    {
      PetscInt localm;
      PetscScalar *Dar;
      PetscScalar *Ear;
      PetscBool passed = PETSC_TRUE, passedGlobal = PETSC_TRUE;
      PetscScalar margin;

      ierr = MatGetLocalSize(D, &localm, NULL); CHKERRQ(ierr);
      ierr = MatDenseGetArray(D, &Dar); CHKERRQ(ierr);
      ierr = MatDenseGetArray(E, &Ear); CHKERRQ(ierr);
      margin = PetscPowReal(((PetscReal) 1. + (PetscReal) PETSC_MACHINE_EPSILON),2. * (2. * r + 1)) - 1.;
      for (i = 0; i < localm * n; i++) {
        PetscReal res = Dar[i];
        PetscReal thresh = margin * Ear[i];
        if (PetscAbsReal(res) > thresh) {
          PetscPrintf(PETSC_COMM_SELF, "Test %D failed error test at local index %D threshold %g with value %g\n", test, i, (double) thresh, (double) res); CHKERRQ(ierr);
          passed = PETSC_FALSE;
          break;
        }
      }
      ierr = MatDenseRestoreArray(E, &Ear); CHKERRQ(ierr);
      ierr = MatDenseRestoreArray(D, &Dar); CHKERRQ(ierr);
      ierr = MPI_Allreduce(&passed, &passedGlobal, 1, MPIU_BOOL, MPI_LAND, comm); CHKERRQ(ierr);
      if (passedGlobal) {
        ierr = PetscViewerASCIIPrintf(viewer, "Test results: passed, average time (%D tests) %g, ***FMAs per second %g***\n", nCheck, totalTime, (double) m * (double) n * (double) r / totalTime);CHKERRQ(ierr);
        fmaps[test] = (double) m * (double) n * (double) r / totalTime;
      } else {
        ierr = PetscViewerASCIIPrintf(viewer, "Test results: FAILED, average time (%D tests) %g, ***FMAs per second 0***\n", nCheck, totalTime);CHKERRQ(ierr);
        fmaps[test] = 0.;
      }
    }
    ierr = MatDestroy(&E); CHKERRQ(ierr);
    ierr = MatDestroy(&D); CHKERRQ(ierr);

    ierr = PetscViewerASCIIPopTab(PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
  }
  ierr = PetscViewerASCIIPopTab(PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
  ierr = PetscRandomDestroy(&selfrand);CHKERRQ(ierr);
  ierr = PetscRandomDestroy(&globalrand);CHKERRQ(ierr);

  ierr = MMMADestroy(&mmma); CHKERRQ(ierr);

  {
    double harmonicMean = 0.;

    for (test = 0; test < numTests; test++) {
      harmonicMean += 1./(fmaps[test] + PETSC_SMALL);
    }
    harmonicMean = numTests / harmonicMean;

    ierr = PetscViewerASCIIPrintf(viewer, "===Harmonic mean: %g FMAs per second===\n", harmonicMean); CHKERRQ(ierr);
  }

  ierr = PetscFinalize();
  return ierr;
}
