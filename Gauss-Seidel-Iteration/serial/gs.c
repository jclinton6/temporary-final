#include "gs.h"
#include <stdlib.h>
#include <stdio.h>

int gauss_seidel_iteration(size_t n, int n_iter, const double *f, double *u)
{

  double *u_ghost = malloc((n+1)*(n+1)*(n+1)*sizeof(double)); // equivalent to u vector but includes boundary cells that will always = 0.0
  int i,j,k,im1,jm1,km1,ip1,jp1,kp1,iter; // indexing variables
  double K_II, K_IJ, b_I; // update variables

  double coeff1 = 8./(3.*n), coeff2 = -1./(6.*n), coeff3 = -1./(12.*n);

  for (i=0;i<((n+1)*(n+1)*(n+1));i++) {
    u_ghost[i] = 0.; // initialize to 0.0
  }

  // populating the interior of u_ghost by only iterating from 1:n
  for (i=1; i<n; i++) {
    for (j=1; j<n; j++) {
      for (k=1; k<n; k++) {
        // indexes of u vector will be one less than the inexes of u_ghost b/c of ghost cell layer
        im1 = i-1;
        jm1 = j-1;
        km1 = k-1;
        // populate u_ghost interior with ghost
        u_ghost[k+(n+1)*(j+(n+1)*i)] = u[km1+(n-1)*(jm1+(n-1)*im1)];
      }
    }
  }

  // loop for n_iter iterations through the interior points
  for (iter=0; iter < n_iter; iter++) {
    for (i=1; i<n; i++) {
      for (j=1; j<n; j++) {
        for (k=1; k<n; k++) {
          ip1 = i+1;
          im1 = i-1;
          jp1 = j+1;
          jm1 = j-1;
          kp1 = k+1;
          km1 = k-1;
          // 21-point stencil can be used at each point becuase ghost cells ensure BCs are met
          K_II = coeff1;
          K_IJ = (coeff2)*(u_ghost[kp1+(n+1)*(jp1+(n+1)*i  )]) + // edge 1
                 (coeff2)*(u_ghost[km1+(n+1)*(jp1+(n+1)*i  )]) + // edge 2
                 (coeff2)*(u_ghost[kp1+(n+1)*(jm1+(n+1)*i  )]) + // edge 3
                 (coeff2)*(u_ghost[km1+(n+1)*(jm1+(n+1)*i  )]) + // edge 4
                 (coeff2)*(u_ghost[kp1+(n+1)*(j  +(n+1)*ip1)]) + // edge 5
                 (coeff2)*(u_ghost[km1+(n+1)*(j  +(n+1)*ip1)]) + // edge 6
                 (coeff2)*(u_ghost[kp1+(n+1)*(j  +(n+1)*im1)]) + // edge 7
                 (coeff2)*(u_ghost[km1+(n+1)*(j  +(n+1)*im1)]) + // edge 8
                 (coeff2)*(u_ghost[k  +(n+1)*(jp1+(n+1)*ip1)]) + // edge 9
                 (coeff2)*(u_ghost[k  +(n+1)*(jm1+(n+1)*ip1)]) + // edge 10
                 (coeff2)*(u_ghost[k  +(n+1)*(jp1+(n+1)*im1)]) + // edge 11
                 (coeff2)*(u_ghost[k  +(n+1)*(jm1+(n+1)*im1)]) + // edge 12
                 (coeff3)*(u_ghost[kp1+(n+1)*(jp1+(n+1)*ip1)]) + // corner 1
                 (coeff3)*(u_ghost[km1+(n+1)*(jp1+(n+1)*ip1)]) + // corner 2
                 (coeff3)*(u_ghost[kp1+(n+1)*(jm1+(n+1)*ip1)]) + // corner 3
                 (coeff3)*(u_ghost[km1+(n+1)*(jm1+(n+1)*ip1)]) + // corner 4
                 (coeff3)*(u_ghost[km1+(n+1)*(jm1+(n+1)*im1)]) + // corner 5
                 (coeff3)*(u_ghost[kp1+(n+1)*(jp1+(n+1)*im1)]) + // corner 6
                 (coeff3)*(u_ghost[km1+(n+1)*(jp1+(n+1)*im1)]) + // corner 7
                 (coeff3)*(u_ghost[kp1+(n+1)*(jm1+(n+1)*im1)]);  // corner 8
          // solution at index taken from f vector
          b_I = f[km1 + (n-1)*(jm1 + (n-1)*im1)];
          // update according to u_I(t+1) = (1/K_II)*(v_I - sum_j=/=i(K_IJ*u_J))
          u_ghost[k + (n+1)*(j + (n+1)*i)] = (1./K_II)*(b_I - K_IJ);
        }
      }
    }
  }

  // re-populate u once iterations are done
  for (i=1; i<n; i++) {
    for (j=1; j<n; j++) {
      for (k=1; k<n; k++) {
        im1 = i-1;
        jm1 = j-1;
        km1 = k-1;
        u[km1+(n-1)*(jm1+(n-1)*im1)] = u_ghost[k+(n+1)*(j+(n+1)*i)];
      }
    }
  }

  return 0;
}

int gauss_seidel_ordering(size_t n, size_t *order)
{
  for (size_t i = 0; i < (n - 1) * (n - 1) * (n - 1); i++) {
    order[i] = i;
  }

  return 0;
}
