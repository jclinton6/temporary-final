const char help[] = "Test driver for the correctness of Gauss-Seidel iteration implementation";

#include <petscmat.h>
#include "gs.h"

static PetscErrorCode PetscGaussSeidelCreateCheckMat(MPI_Comm comm, PetscInt debug, PetscInt n, struct IndexBlock *ib, Mat *checkMat)
{
  PetscInt       nm1 = n - 1;
  PetscInt       N = nm1 * nm1 * nm1;
  PetscReal      h = 1. / n;
  PetscInt       i, j, k, l;
  Mat            K;
  size_t         xS = ib->xStart;
  size_t         xE = ib->xEnd;
  size_t         yS = ib->yStart;
  size_t         yE = ib->yEnd;
  size_t         zS = ib->zStart;
  size_t         zE = ib->zEnd;
  PetscInt       nx = PetscMax(xE - xS,0);
  PetscInt       ny = PetscMax(yE - yS,0);
  PetscInt       nz = PetscMax(zE - zS,0);
  PetscInt       nloc = nx * ny * nz;
  PetscMPIInt    size, rank;
  struct IndexBlock *ibAll;
  PetscInt       *procOffsets, myOffset;
  PetscInt       *haloLocalToGlobal[3][3][3];
  PetscInt       haloDim[3][3][3][3];
  PetscScalar    v[27] = {-1./12., -1./6. , -1./12.,
                          -1./6. ,  0.    , -1./6. ,
                          -1./12., -1./6. , -1./12.,
                          -1./6. ,  0.    , -1./6. ,
                          0.    ,  8./3. ,  0.    ,
                          -1./6. ,  0.    , -1./6. ,
                          -1./12., -1./6. , -1./12.,
                          -1./6. ,  0.    , -1./6. ,
                          -1./12., -1./6. , -1./12.};
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = MPI_Comm_size(comm, &size); CHKERRQ(ierr);
  ierr = MPI_Comm_rank(comm, &rank); CHKERRQ(ierr);
  ierr = PetscMalloc2(size,&ibAll,size+1,&procOffsets); CHKERRQ(ierr);
  /* Get the block sizes of all processes */
  ierr = MPI_Allgather(ib, sizeof(*ib), MPI_BYTE, ibAll, sizeof(*ib), MPI_BYTE, comm); CHKERRQ(ierr);
  ierr = MPI_Allgather(&nloc, 1, MPIU_INT, procOffsets, 1, MPIU_INT, comm); CHKERRQ(ierr);
  {
    PetscInt offset = 0;
    for (i = 0; i < size; i++) {
      PetscInt newOffset = offset + procOffsets[i];

      procOffsets[i] = offset;
      offset = newOffset;
    }
    procOffsets[size] = N;
    myOffset = procOffsets[rank];
  }
  /* Create arrays of the global indices for the halos */
  for (k = 0; k < 3; k++) {
    PetscInt haloZS = -1, haloZE = -1;

    if (k == 0 && zS > 0) {
      haloZS = zS - 1;
      haloZE = zS;
    } else if (k == 2 && zE < n - 1) {
      haloZS = zE;
      haloZE = zE + 1;
    } else if (k == 1 && zE > zS) {
      haloZS = zS;
      haloZE = zE;
    }
    for (j = 0; j < 3; j++) {
      PetscInt haloYS = -1, haloYE = -1;

      if (j == 0 && yS > 0) {
        haloYS = yS - 1;
        haloYE = yS;
      } else if (j == 2 && yE < n - 1) {
        haloYS = yE;
        haloYE = yE + 1;
      } else if (j == 1 && yE > yS) {
        haloYS = yS;
        haloYE = yE;
      }

      for (i = 0; i < 3; i++) {
        PetscInt haloXS = -1, haloXE = -1;
        PetscInt haloSize, hi, hj, hk;
        PetscInt *halo;

        haloDim[k][j][i][0] = 0;
        haloDim[k][j][i][1] = 0;
        haloDim[k][j][i][2] = 0;
        if (k == 1 && j == 1 && i == 1) {
          haloLocalToGlobal[k][j][i] = NULL;
          continue;
        }

        if (i == 0 && xS > 0) {
          haloXS = xS - 1;
          haloXE = xS;
        } else if (i == 2 && xE < n - 1) {
          haloXS = xE;
          haloXE = xE + 1;
        } else if (i == 1 && xE > xS) {
          haloXS = xS;
          haloXE = xE;
        }

        haloDim[k][j][i][0] = haloXE - haloXS;
        haloDim[k][j][i][1] = haloYE - haloYS;
        haloDim[k][j][i][2] = haloZE - haloZS;

        haloSize = (haloXE - haloXS) * (haloYE - haloYS) * (haloZE - haloZS);
        ierr = PetscMalloc1(haloSize,&haloLocalToGlobal[k][j][i]); CHKERRQ(ierr);
        if (!haloSize) continue;
        if (debug == rank) {
          PetscPrintf(PETSC_COMM_SELF, "[%D] halo (%D,%D,%D) is [%D,%D)x[%D,%D)x[%D,%D)\n", rank, i, j, k, haloXS, haloXE, haloYS, haloYE, haloZS, haloZE); CHKERRQ(ierr);
        }

        halo = &haloLocalToGlobal[k][j][i][0];
        /* Initialize all halo global indices to -1 so that we know if they
         * have been set or not */
        for (l = 0; l < haloSize; l++) {
          halo[l] = -1;
        }

        for (l = 0, hk = 0; hk < (haloZE - haloZS); hk++) {
          PetscInt globalZ = haloZS + hk;
          for (hj = 0; hj < (haloYE - haloYS); hj++) {
            PetscInt globalY = haloYS + hj;
            for (hi = 0; hi < (haloXE - haloXS); hi++, l++) {
              PetscInt globalX = haloXS + hi, p;
              PetscInt gXS, gXE, gYS, gYE, gZS, gZE;
              PetscInt gi, gj, gk, pSize;
              struct IndexBlock *pib = NULL;
              if (halo[l] >= 0) continue;

              /* If this halo index hasn't been set, find the process that
               * owns it */
              for (p = 0; p < size; p++) {
                pib = &ibAll[p];
                if (pib->xStart <= globalX && globalX < pib->xEnd &&
                    pib->yStart <= globalY && globalY < pib->yEnd &&
                    pib->zStart <= globalZ && globalZ < pib->zEnd) {
                  break;
                }
              }
              if (p == size) SETERRQ3(PETSC_COMM_SELF, PETSC_ERR_ARG_OUTOFRANGE, "Could not find processor that owns (%D, %D, %D)\n", globalX, globalY, globalZ);

              /* Once we find a process, set all of the indices in the halo
               * that are owned by that process */
              pSize = PetscMax(pib->xEnd - pib->xStart,0) * PetscMax(pib->yEnd - pib->yStart,0) * PetscMax(pib->zEnd - pib->zStart,0);
              if (procOffsets[p] + pSize != procOffsets[p + 1]) SETERRQ3(PETSC_COMM_SELF, PETSC_ERR_PLIB, "Offsets and sizes do not agree for process %D: %D vs %D\n", p,pSize,procOffsets[p+1]-procOffsets[p]);

              gXS = PetscMax(pib->xStart, haloXS);
              gXE = PetscMin(pib->xEnd, haloXE);
              gYS = PetscMax(pib->yStart, haloYS);
              gYE = PetscMin(pib->yEnd, haloYE);
              gZS = PetscMax(pib->zStart, haloZS);
              gZE = PetscMin(pib->zEnd, haloZE);
              if (debug == rank) {
                PetscPrintf(PETSC_COMM_SELF, "[%D] halo (%D,%D,%D) overlaps process %D at:\n", rank, i, j, k, p); CHKERRQ(ierr);
                PetscPrintf(PETSC_COMM_SELF, "  global coordinates [%D,%D)x[%D,%D)x[%D,%D)\n", gXS, gXE, gYS, gYE, gZS, gZE); CHKERRQ(ierr);
                PetscPrintf(PETSC_COMM_SELF, "    halo coordinates [%D,%D)x[%D,%D)x[%D,%D)\n", gXS - haloXS, gXE - haloXS, gYS - haloYS, gYE - haloYS, gZS - haloZS, gZE - haloZS); CHKERRQ(ierr);
                PetscPrintf(PETSC_COMM_SELF, "  remote coordinates [%D,%D)x[%D,%D)x[%D,%D)\n", gXS - pib->xStart, gXE - pib->xStart, gYS - pib->yStart, gYE - pib->yStart, gZS - pib->zStart, gZE - pib->zStart); CHKERRQ(ierr);
              }
              for (gk = gZS; gk < gZE; gk++) {
                PetscInt pk = gk - pib->zStart;
                PetscInt hpk = gk - haloZS;

                for (gj = gYS; gj < gYE; gj++) {
                  PetscInt pj = gj - pib->yStart;
                  PetscInt hpj = gj - haloYS;

                  for (gi = gXS; gi < gXE; gi++) {
                    PetscInt pi = gi - pib->xStart;
                    PetscInt hpi = gi - haloXS;
                    PetscInt pIndex, hIndex;

                    pIndex = pi + (pib->xEnd - pib->xStart) * (pj + (pib->yEnd - pib->yStart) * pk);
                    if (pIndex < 0 || pIndex >= pSize) SETERRQ5(PETSC_COMM_SELF, PETSC_ERR_PLIB, "Did not compute valid local index (%D) for (%D, %D, %D) on process (%D)\n", pIndex, gi, gj, gk, p);
                    hIndex = hpi + (haloXE - haloXS) * (hpj + (haloYE - haloYS) * hpk);
                    if (hIndex < 0 || hIndex >= haloSize) SETERRQ5(PETSC_COMM_SELF, PETSC_ERR_PLIB, "Did not compute valid halo index (%D) for (%D, %D, %D) in halo (%D)\n", hIndex, gi, gj, gk, l);
                    halo[hIndex] = pIndex + procOffsets[p];
                  }
                }
              }
            }
          }
        }
        for (l = 0; l < haloSize; l++) {
          if (halo[l] < 0 || halo[l] >= N) SETERRQ2(PETSC_COMM_SELF, PETSC_ERR_PLIB, "Invalid index (%D) computed for halo position %D\n", halo[l], l);
        }
      }
    }
  }

  ierr = MatCreate(comm, &K);CHKERRQ(ierr);
  ierr = MatSetType(K, MATMPIAIJ);CHKERRQ(ierr);
  ierr = MatSetSizes(K, nloc, nloc, N, N);CHKERRQ(ierr);
  { /* determine how many matrix entries are in this process's diagonal block, versus the number that will be communicated */
    PetscInt *dnnz, *onnz;

    ierr = PetscMalloc2(nloc, &dnnz, nloc, &onnz); CHKERRQ(ierr);
    for (l = 0, k = ib->zStart; k < ib->zEnd; k++) {
      for (j = ib->yStart; j < ib->yEnd; j++) {
        for (i = ib->xStart; i < ib->xEnd; i++, l++) {
          PetscInt ni, nj, nk;
          PetscInt o = 0, d = 0;
          for (nk = k - 1; nk <= k + 1; nk++) {
            for (nj = j - 1; nj <= j + 1; nj++) {
              for (ni = i - 1; ni <= i + 1; ni++) {
                if (nk < (PetscInt) ib->zStart || nk >= (PetscInt) ib->zEnd ||
                    nj < (PetscInt) ib->yStart || nj >= (PetscInt) ib->yEnd ||
                    ni < (PetscInt) ib->xStart || ni >= (PetscInt) ib->xEnd) {
                  if (nk >= 0 && nk < nm1 &&
                      nj >= 0 && nj < nm1 &&
                      ni >= 0 && ni < nm1) {
                    o++;
                  }
                } else {
                  d++;
                }
              }
            }
          }
          dnnz[l] = d;
          onnz[l] = o;
        }
      }
    }
    ierr = MatMPIAIJSetPreallocation(K, 0, dnnz, 0, onnz); CHKERRQ(ierr);
    ierr = PetscFree2(dnnz, onnz);
  }

  /* Now fill in the entries, using the halo arrays */
  for (l = 0; l < 27; l++) v[l] *= h;
  for (l = 0, k = ib->zStart; k < ib->zEnd; k++) {
    for (j = ib->yStart; j < ib->yEnd; j++) {
      for (i = ib->xStart; i < ib->xEnd; i++, l++) {
        PetscInt ni, nj, nk, nl;
        PetscInt indices[27];
        PetscInt self;

        for (nl = 0, nk = k - 1; nk <= k + 1; nk++) {
          PetscInt hk = nk < (PetscInt) ib->zStart ? 0 : nk < (PetscInt) ib->zEnd ? 1 : 2;
          for (nj = j - 1; nj <= j + 1; nj++) {
            PetscInt hj = nj < (PetscInt) ib->yStart ? 0 : nj < (PetscInt) ib->yEnd ? 1 : 2;
            for (ni = i - 1; ni <= i + 1; ni++, nl++) {
              PetscInt hi = ni < (PetscInt) ib->xStart ? 0 : ni < (PetscInt) ib->xEnd ? 1 : 2;
              PetscInt index;
              PetscInt *hDim = &haloDim[hk][hj][hi][0];
              PetscInt *halo = &haloLocalToGlobal[hk][hj][hi][0];

              index = -1;
              if (hk == 1 && hj == 1 && hi == 1) {
                index = myOffset + (ni - ib->xStart) + nx * ((nj - ib->yStart) + ny * (nk - ib->zStart));

                if (index < procOffsets[rank] || index >= procOffsets[rank + 1]) SETERRQ4(PETSC_COMM_SELF, PETSC_ERR_PLIB, "Invalid index (%D) computed for (%D, %D, %D)\n", index, ni, nj, nk);
              } else if (hDim[0] && hDim[1] && hDim[2]) {
                PetscInt hni, hnj, hnk;

                hni = (hi == 1) ? (ni - ib->xStart) : 0;
                hnj = (hj == 1) ? (nj - ib->yStart) : 0;
                hnk = (hk == 1) ? (nk - ib->zStart) : 0;
                index = halo[hni + hDim[0] * (hnj + hDim[1] * hnk)];
                if (index < 0 || index >= N) SETERRQ4(PETSC_COMM_SELF, PETSC_ERR_PLIB, "Invalid index (%D) computed for (%D, %D, %D)\n", index, ni, nj, nk);
              }
              indices[nl] = index;
            }
          }
        }
        self = indices[13];
        if (self < procOffsets[rank] || self >= procOffsets[rank + 1]) SETERRQ1(PETSC_COMM_SELF, PETSC_ERR_PLIB, "Index %D introducing off process entries\n", self);
        ierr = MatSetValues(K, 1, &self, 27, indices, v, INSERT_VALUES);CHKERRQ(ierr);
      }
    }
  }

  ierr = MatAssemblyBegin(K, MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);
  ierr = MatAssemblyEnd(K, MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);

  /* Clean up */
  for (k = 0; k < 3; k++) {
    for (j = 0; j < 3; j++) {
      for (i = 0; i < 3; i++) {
        ierr = PetscFree(haloLocalToGlobal[k][j][i]); CHKERRQ(ierr);
      }
    }
  }
  ierr = PetscFree2(ibAll,procOffsets); CHKERRQ(ierr);
  *checkMat = K;
  PetscFunctionReturn(0);
}

static PetscErrorCode GSTest(MPI_Comm comm, GS gs, PetscInt n, PetscInt nits, struct IndexBlock *ib, double *f, double *u, Mat checkMat, Vec uVec, Vec fVec, PetscInt nCheck, double *avgTime, double *relErr)
{
  double totalTime = 0.;
  PetscInt     i, localSize;
  const PetscScalar *fArray, *uArray;
  PetscScalar *eArray, errorNorm, uNorm;
  PetscErrorCode ierr;
  PetscFunctionBegin;

  ierr = VecGetLocalSize(uVec, &localSize); CHKERRQ(ierr);
  ierr = MatMult(checkMat, uVec, fVec); CHKERRQ(ierr);
  ierr = VecNorm(uVec,NORM_2,&uNorm); CHKERRQ(ierr);
  ierr = VecGetArrayRead(uVec, &uArray); CHKERRQ(ierr);
  ierr = VecGetArrayRead(fVec, &fArray); CHKERRQ(ierr);
  for (int i = 0; i < nCheck + 1; i++) {
    double tic, toc, total, maxTotal = 0.;

    memcpy(f, fArray, localSize * sizeof(double)); CHKERRQ(ierr);
    memset(u, 0, localSize * sizeof(double)); CHKERRQ(ierr);
    ierr = MPI_Barrier(comm); CHKERRQ(ierr);
    tic = MPI_Wtime();
    ierr = GSIterate(gs, n, nits, ib, f, u); CHKERRQ(ierr);
    toc = MPI_Wtime();
    total = toc - tic;
    ierr = MPI_Allreduce(&total, &maxTotal, 1, MPI_DOUBLE, MPI_MAX, comm); CHKERRQ(ierr);
    if (i) {totalTime += maxTotal;}
  }
  ierr = VecRestoreArrayRead(fVec, &fArray); CHKERRQ(ierr);
  ierr = VecGetArray(fVec,&eArray); CHKERRQ(ierr);
  for (i = 0; i < localSize; i++) {
    eArray[i] = uArray[i] - u[i];
  }
  ierr = VecRestoreArray(fVec,&eArray); CHKERRQ(ierr);
  ierr = VecRestoreArrayRead(uVec, &uArray); CHKERRQ(ierr);
  ierr = VecNorm(fVec, NORM_2, &errorNorm); CHKERRQ(ierr);
  *relErr = errorNorm / uNorm;
  *avgTime = totalTime / nCheck;
  PetscFunctionReturn(0);
}

int main(int argc, char **argv)
{
  PetscInt       tests[4] = {17, 65, 257, 1025}, test;
  PetscScalar    edps[4];
  PetscInt       numTests = 3;//4;
  PetscInt       nits = 3, nCheck = 10;
  PetscInt       debug = -1;
  PetscRandom    rand;
  MPI_Comm       comm;
  PetscViewer    viewer;
  PetscErrorCode ierr;

  ierr = PetscInitialize(&argc, &argv, NULL, help); if (ierr) return ierr;
  comm = PETSC_COMM_WORLD;
  viewer = PETSC_VIEWER_STDOUT_WORLD;
  ierr = PetscOptionsBegin(comm, NULL, "Gauss Seidel Iteration Test Options", "test_gs.c");CHKERRQ(ierr);
  ierr = PetscOptionsIntArray("-tests", "Test sizes to run", "test_gs.c", tests, &numTests, NULL);CHKERRQ(ierr);
  ierr = PetscOptionsInt("-num_its", "The number of iterations per test", "test_gs.c", nits, &nits, NULL);CHKERRQ(ierr);
  ierr = PetscOptionsInt("-num_time", "The number of times a test is timed", "test_gs.c", nCheck, &nCheck, NULL);CHKERRQ(ierr);
  ierr = PetscOptionsInt("-debug", "debug statements for this rank", "test_gs.c", debug, &debug, NULL);CHKERRQ(ierr);
  ierr = PetscOptionsEnd();CHKERRQ(ierr);

  if (!numTests) {numTests = 3;}//4;}

  ierr = PetscRandomCreate(comm, &rand);CHKERRQ(ierr);
  ierr = PetscRandomSetFromOptions(rand);CHKERRQ(ierr);
  ierr = PetscRandomSeed(rand);CHKERRQ(ierr);

  ierr = PetscViewerASCIIPrintf(viewer, "Running %D tests of Gauss Seidel iteration\n", numTests);CHKERRQ(ierr);
  ierr = PetscViewerASCIIPushTab(PETSC_VIEWER_STDOUT_WORLD);
  for (test = 0; test < numTests; test++) {
    PetscScalar *f, *u;
    PetscInt    n;
    GS          gs = NULL;
    Mat         checkMat = NULL;
    Vec         fVec, uVec;
    struct IndexBlock ib;
    double      avgTime, relErr;
    PetscInt    localSize;

    ierr = PetscViewerASCIIPrintf(viewer, "Test %D:\n", test);CHKERRQ(ierr);
    ierr = PetscViewerASCIIPushTab(PETSC_VIEWER_STDOUT_WORLD);

    ierr = GSCreate(comm, &gs); CHKERRQ(ierr);

    n = tests[test];

    ierr = PetscViewerASCIIPrintf(viewer, "Test dimensions: %D cells (%D dofs) per dimension, %D iterations\n", n, n-1, nits);CHKERRQ(ierr);
    ierr = GSGetFieldArrays(gs, n, &ib, &f, &u); CHKERRQ(ierr);

    ierr = PetscGaussSeidelCreateCheckMat(PETSC_COMM_WORLD, debug, n, &ib, &checkMat);CHKERRQ(ierr);
    ierr = MatCreateVecs(checkMat,&fVec,&uVec); CHKERRQ(ierr);
    ierr = VecGetLocalSize(uVec,&localSize); CHKERRQ(ierr);

    ierr = PetscViewerASCIIPrintf(viewer, "Test random solution, %D instances of %D iterations\n", nCheck, nits);CHKERRQ(ierr);
    ierr = PetscViewerASCIIPushTab(PETSC_VIEWER_STDOUT_WORLD);

    ierr = PetscRandomSetInterval(rand, -1., 1.);CHKERRQ(ierr);
    {
      PetscScalar *uArray;
      PetscInt     i;

      ierr = VecGetArray(uVec, &uArray); CHKERRQ(ierr);
      for (i = 0; i < localSize ; i++) {
        ierr = PetscRandomGetValue(rand, &uArray[i]);CHKERRQ(ierr);
      }
      ierr = VecRestoreArray(uVec, &uArray); CHKERRQ(ierr);
    }
    ierr = GSTest(comm, gs, n, nits, &ib, f, u, checkMat, uVec, fVec, nCheck, &avgTime, &relErr); CHKERRQ(ierr);
    ierr = PetscViewerASCIIPrintf(viewer, "Test %D, random solution, %D dofs per dimension: avg %g seconds for %D iterations, relative error %g, ***%g equation digits solved per second***\n", test, n - 1, (double) avgTime, nits, (double) relErr, (double) -log10(relErr) * (n-1)*(n-1)*(n-1) / avgTime);CHKERRQ(ierr);
    ierr = PetscViewerASCIIPopTab(PETSC_VIEWER_STDOUT_WORLD);

    edps[test] = -log10(relErr) * (n-1)*(n-1)*(n-1) / avgTime;

#if 0
    ierr = PetscViewerASCIIPrintf(viewer, "Test low freq. soln., %D instances of %D iterations\n", nCheck, nits);CHKERRQ(ierr);
    ierr = PetscViewerASCIIPushTab(PETSC_VIEWER_STDOUT_WORLD);

    {
      PetscScalar *uArray;
      PetscInt     i, j, k, l;

      ierr = VecGetArray(uVec, &uArray); CHKERRQ(ierr);

      for (l = 0, k = ib.zStart; k < ib.zEnd; k++) {
        double zpos = (double) (k + 1) / (double) n;
        double zmul = sin(zpos * PETSC_PI);

        for (j = ib.yStart; j < ib.yEnd; j++) {
          double ypos = (double) (j + 1) / (double) n;
          double ymul = sin(ypos * PETSC_PI);

          for (i = ib.xStart; i < ib.xEnd; i++, l++) {
            double xpos = (double) (i + 1) / (double) n;
            double xmul = sin(xpos * PETSC_PI);

            uArray[l] = zmul * ymul * xmul;
          }
        }
      }
      ierr = VecRestoreArray(uVec, &uArray); CHKERRQ(ierr);
    }
    ierr = GSTest(comm, gs, n, nits, &ib, f, u, checkMat, uVec, fVec, nCheck, &avgTime, &relErr); CHKERRQ(ierr);
    ierr = PetscViewerASCIIPrintf(viewer, "Test %D, low freq. soln., %D dofs per dimension: avg %g seconds for %D iterations, relative error %g, ***%g equation digits solved per second***\n", test, n - 1, (double) avgTime, nits, (double) relErr, (double) -log10(relErr) * (n-1)*(n-1)*(n-1) / avgTime);CHKERRQ(ierr);
    ierr = PetscViewerASCIIPrintf(viewer, "[%g scaled convergence rate]\n", (double) (relErr - pow(1. - 16. * pow((double)n,-1.93),nits))/ relErr); CHKERRQ(ierr);
    ierr = PetscViewerASCIIPopTab(PETSC_VIEWER_STDOUT_WORLD); CHKERRQ(ierr);
#endif

    ierr = VecDestroy(&fVec);CHKERRQ(ierr);
    ierr = VecDestroy(&uVec);CHKERRQ(ierr);
    ierr = MatDestroy(&checkMat);CHKERRQ(ierr);

    ierr = GSRestoreFieldArrays(gs, n, &ib, &f, &u); CHKERRQ(ierr);

    ierr = GSDestroy(&gs);CHKERRQ(ierr);

    ierr = PetscViewerASCIIPopTab(PETSC_VIEWER_STDOUT_WORLD);
  }
  ierr = PetscViewerASCIIPopTab(PETSC_VIEWER_STDOUT_WORLD);
  ierr = PetscRandomDestroy(&rand);CHKERRQ(ierr);

  {
    double harmonicMean = 0.;

    for (test = 0; test < numTests; test++) {
      harmonicMean += 1./(edps[test] + PETSC_SMALL);
    }
    harmonicMean = numTests / harmonicMean;

    ierr = PetscViewerASCIIPrintf(viewer, "===Harmonic mean: %g equation digits solved per second===\n", harmonicMean); CHKERRQ(ierr);
  }

  ierr = PetscFinalize();
  return ierr;
}
