#!/bin/sh
#SBATCH  -J gs_test                    # Job name
#SBATCH  -p development                # Queue (development or normal)
#SBATCH  -N 1                          # Number of nodes
#SBATCH --tasks-per-node 64            # Number of tasks per node
#SBATCH  -t 00:02:00                   # Time limit hrs:min:sec
#SBATCH  -A TG-TRA170035               # My node alone
#SBATCH  -o gs-%j.out                  # Standard output and error log

pwd; hostname; date

ibrun ./test_gs

date
