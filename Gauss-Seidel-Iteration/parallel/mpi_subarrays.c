#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <mpi.h>

int main(int argc, char** argv) {

  int          ierr, size, rank, sendto, recvfrom,
               N, i, j, k, p;
  int    ndims=3, size_arr[3], subsize_arr[3], start_arr[3]; 
  double       *arr, *recvbuf, *recvfull, *expected_sub;
  MPI_Datatype mysubarray;
  MPI_Request sendreq, recvreq;
  MPI_Status stat;

  // MPI environment setup
  ierr = MPI_Init(NULL, NULL); //&argc, &argv);
  ierr = MPI_Comm_size(MPI_COMM_WORLD, &size);
  ierr = MPI_Comm_rank(MPI_COMM_WORLD, &rank);

  if (argc != 2) {
    printf("ERROR:\n\t Correct usage:\n\t<executable> DIM_OF_PROC_ARRAY\n");
    return 0;
  }
  // array will be NxNxN
  N = atoi(argv[1]);
  // who will I be sending to and receiving from
  if (rank == size - 1) {
    sendto  = 0;
    recvfrom = rank - 1;
  } else if (rank == 0) {
    sendto  = rank + 1;
    recvfrom = size - 1;
  } else {
    sendto  = rank + 1;
    recvfrom = rank - 1;
  }
  // initialize each proc's array to unique values
  arr      = malloc(N*N*N*sizeof(double));
  recvfull = malloc(N*N*N*sizeof(double));
  for (i = 0; i < N*N*N - 1; i++) {
    arr[i]      = (i+1)*(rank+1);
    recvfull[i] = (i+1)*(recvfrom+1);
  }
  expected_sub  = malloc((N-2)*(N-2)*sizeof(double));
  recvbuf       = malloc((N-2)*(N-2)*sizeof(double));
  for (k=0; k<(N-2); k++) {
    for (j=0; j<(N-2); j++) {
      expected_sub[j + (N-2)*k] = recvfull[(N-2) + N*((j+1) + N*(k+1))];
    }
  }
  free(recvfull);
  // we want to send the plane constant i = imax-1 with j&k
  // values starting from the second index to one below the max 
  size_arr[0]    = N;
  size_arr[1]    = N;
  size_arr[2]    = N;
  subsize_arr[0] = N - 2;
  subsize_arr[1] = N - 2;
  subsize_arr[2] = 1;
  start_arr[0]   = 1;
  start_arr[1]   = 1;
  start_arr[2]   = N - 2;
  // create subarray
  ierr = MPI_Type_create_subarray(ndims, size_arr, subsize_arr, start_arr,
                           MPI_ORDER_C, MPI_DOUBLE, &mysubarray);
  ierr = MPI_Type_commit(&mysubarray);
  // non-blocking send and receive
  ierr = MPI_Isend(    arr,           1, mysubarray,   sendto, 0, MPI_COMM_WORLD, &sendreq);
  ierr = MPI_Irecv(recvbuf, (N-2)*(N-2), MPI_DOUBLE, recvfrom, 0, MPI_COMM_WORLD, &recvreq);
  ierr = MPI_Wait(&recvreq, &stat);
  ierr = MPI_Wait(&sendreq, &stat);
  // check results
  for (p=0; p<size; p++) {
    if (p == rank) {
      printf("\nProcess %d\n", rank);
      for (k=0; k<(N-2); k++) {
        for (j=0; j<(N-2); j++) {
          printf("j=%d,k=%d\t\tSent:%f\tExpected:%f\tReceived:%f\n",j,k,arr[(N-2) + N*((j+1) + N*(k+1))],expected_sub[j+(N-2)*k],recvbuf[j+(N-2)*k]);
        }
      }
    sleep(3);
    }
    MPI_Barrier(MPI_COMM_WORLD);
  }

  MPI_Finalize();
  return 0;

}
