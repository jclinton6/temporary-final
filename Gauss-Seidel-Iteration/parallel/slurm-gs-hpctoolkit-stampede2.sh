#!/bin/sh
#SBATCH  -J gs                           # Job name
#SBATCH  -p development                  # Queue (development or normal)
#SBATCH  -N 1                            # Number of nodes
#SBATCH --tasks-per-node 27              # Number of tasks per node
#SBATCH  -t 00:02:00                     # Time limit hrs:min:sec
#SBATCH  -A TG-TRA170035                 # My node alone
#SBATCH  -o gs-%j.out                    # Standard output and error log

module load hpctoolkit

pwd; hostname; date

ibrun tacc_affinity hpcrun -t ./test_gs

date
